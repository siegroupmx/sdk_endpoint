#!/usr/bin/php
<?php
/**
 * Sending RAW data using Token
*/

include( "../autoload.php" );

$user= NULL;
$pass= NULL;
$path= 'facturas/save';
$file= 'mytoken.txt';

if( !file_exists($file) ) {
	echo "\nNo tienes una token aun...";
}
else {
	$token= trim(file_get_contents($file));

	$data= array(
		"a1"=>"cosa1",
		"a2"=>"cosa2",
		"r"=>array(
			"a"=>"hola", 
			"b"=>"que onda"
		), 
		"f"=>"que paso"
	);

	$c= new endpoint( $user, $pass, $token, $path, $data );

	echo "\n\nEjemplo de Transaccion....\n";

	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($c->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($c->getHeaderResponse());
	echo "\n\n==========================\n\n";

	if( $c->getError() ) {
		echo '[Error] '. $c->getError();
	}
	else {
		echo "\nExito:";
		echo "\n\nData en Array:\n";
		print_r($c->getRespuesta());

		echo "\n\nData en JSON:\n";
		print_r($c->getRespuesta("json"));
	}
}
echo "\n\n";
exit(0);
?>
