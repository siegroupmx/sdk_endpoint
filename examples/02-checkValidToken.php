#!/usr/bin/php
<?php
include( "../autoload.php" );

$user= 'democo';
$pass= 'gratis123';
$path= 'cuenta/token/check';
$data= NULL;
$file= 'mytoken.txt';

if( !file_exists($file) ) {
	echo "\nNo tienes una token aun...";
}
else {
	$token= trim(file_get_contents($file));
	$data= array(
		"token"=>$token
	);

	$c= new endpoint( $user, $pass, NULL, $path, $data );

	echo "\n\nEjemplo de Transaccion....\n";

	/* Headers */
	echo "\n\nHeaders Request:\n";
	print_r($c->getHeaderRequest());

	echo "\n\nHeaders Response:\n";
	print_r($c->getHeaderResponse());
	echo "\n\n==========================\n\n";

	if( $c->getError() ) {
		echo '[Error] '. $c->getError();
	}
	else {
		echo "\nExito:";
		echo "\n\nData en Array:\n";
		print_r($c->getRespuesta());

		echo "\n\nData en JSON:\n";
		print_r($c->getRespuesta("json"));
	}
	}
echo "\n\n";
exit(0);
?>
