<?php
class endpoint {
	const API_SERVER= 'https://sandbox.ep.moneybox.business/api.php';
	// const API_SERVER= 'https://api.ep.moneybox.business/api.php';
	// const API_SERVER= 'https://mycompany.ep.moneybox.business/api.php';
	private $headerResponse=NULL;
	private $headerRequest=NULL;
	private $requestData=NULL;
	private $user=NULL;
	private $pass=NULL;
	private $token=NULL;
	private $path=NULL;
	private $error=NULL;
	private $error_code=NULL;

	private function getDataToSend() {
		$r= array(
			"access"=> array( 
				"user"=>$this->user, 
				"pass"=>$this->pass, 
				"token"=>$this->token, 
			), 
			"metodo"=>$this->path, 
			"data"=>$this->requestData
		);

	return json_encode($r);
	}

	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ((is_array($a) && $a["request_header"]) ? $a["request_header"].$this->getDataToSend():NULL);
	}

	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	public function getRespuesta($a=NULL) {
		return (!strcmp(strtolower($a), "json") ? $this->result_json:$this->result);
	}

	public function setRespuesta($a=NULL) {
		# print_r($a);
		$this->result= ($a ? json_decode($a):NULL);
		$this->result_json= ($a ? $a:NULL);

		if( !isset($this->result->result) && !isset($this->result->error) && !isset($this->result->error_code) )
			$this->setError("No se obtuvo respuesta del servidor", "001");
		else if( isset($this->result->error_code) ) {
			$this->setError($this->result->error, $this->result->error_code);
		}
	}

	public function getError() {
		return ($this->error_code ? $this->error_code.' - ':'').$this->error;
	}

	public function setError($a=NULL, $c=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->error_code= ($c ? $c:NULL);
	}

	public function debugResponse() {
		if( !$this->getHeaderResponse() )	$this->setError("no se pudo obtener respuesta del servidor");
		else {
			#echo "\n\n## REQUEST:\n";
			#print_r($this->getHeaderRequest());
			#echo "\n\n## RESPONSE:\n";
			#print_r($this->getHeaderResponse());

			$a= explode( "\r\n\r\n", $this->getHeaderResponse());
			if( !$a[1] ) {
				print_r($a);
				$this->setError("el paquete recibido por el webservice esta vacio");
			}
			else { 	
				if( strstr($a[0], "100 Continue") )
					$this->setRespuesta($a[2]);
				else
					$this->setRespuesta($a[1]);
			}
			unset($a);
		}
	}

	public function sendToEndpoint() {
		$s= curl_init();
		curl_setopt($s, CURLOPT_URL, self::API_SERVER );
		curl_setopt($s, CURLOPT_HTTPHEADER, array("PHP", "Content-Type: application/json; charset=UTF-8") );
		curl_setopt($s, CURLOPT_HEADER, 1 );
		curl_setopt($s, CURLOPT_POST, 1);
		curl_setopt($s, CURLOPT_POSTFIELDS, $this->getDataToSend());
		curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
		/**
		* Habilita las siguientes lineas si tienes fallo en obtener el Certificado
		*/
		curl_setopt($s, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($s, CURLOPT_VERBOSE, TRUE);
		curl_setopt($s, CURLINFO_HEADER_OUT, true);
		$resp= curl_exec($s);
		$rq= curl_getinfo($s);
		$this->setHeaderRequest($rq); // request
		$this->setHeaderResponse($resp); // response
		$this->debugResponse();
		curl_close($s);
		unset($rq, $resp, $s, $schema);
	}

	public function __construct($username=NULL, $password=NULL, $token=NULL, $webServicePathUrl=NULL, $data=NULL) {
		$this->path= ($webServicePathUrl ? $webServicePathUrl:NULL);
		$this->user= ($username ? $username:NULL);
		$this->pass= ($password ? $password:NULL);
		$this->token= ($token ? $token:NULL);
		$this->requestData= ((is_array($data) && count($data)) ? $data:NULL);
		$this->sendToEndpoint();
	}
}
?>
